package biz.blitzdev.monad.sample

import java.util.Properties

import scala.Function.const
import scala.collection.JavaConversions.mapAsJavaMap

import biz.blitzdev.monad.Reader
import biz.blitzdev.monad.Reader.{asks, unit}

object ReaderSample {

	def main(args:Array[String]) {

		// 環境の型は毎行書くので短縮
		type P = Properties;

		// ホームディレクトリ直下の「workspace」ディレクトリのパスを構成
		// Haskellだとコンパイラのサポートがあり毎行「<-」を使うところ
		val workspacePathReader =
				asks((_:P).getProperty("file.separator")).bind(fileSeparator =>
				asks((_:P).getProperty("user.home"     )).bind(userHome      =>
				unit(userHome + fileSeparator + "workspace")))

		// workspacePathReaderの実行前に、
		// 環境 (引数に渡すProperties) にlocalの引数の関数を適用するよう変更
		// 言い換えると、引数の前処理を追加
		val fromMapReader = workspacePathReader.local(
			(map : Map[String, String]) => {
				val ps = new Properties()
				ps.putAll(map)
				ps
			})

		// 使い方その1
		def getWorkspacePathFromProperties(properties : Properties) =
			workspacePathReader.run(properties)

		// 使い方その2
		println("workspace path: " + fromMapReader.run(Map(
				("user.home", "/home/hoge"),
				("file.separator", "/"))))

		// 何を渡しても特定の値 (constの引数) を使用するよう変更
		val constReader = fromMapReader.local(const(Map(
				("user.home", "/home/fuga"),
				("file.separator", "/"))))

		// 使い方その3
		println("workspace path: " + constReader.run("ignored"))
	}
}
