package biz.blitzdev.monad

import scala.Function.const

// A minimal subset of Reader monad in Scala
object Reader {

	// 'return' in Haskell
	def unit[E, T](a : T) : Reader[E, T] =
		new Reader[E, T](const(a))

	def ask[E] : Reader[E, E] =
		new Reader[E, E](e => e)

	def asks[E, T](sel : E => T) : Reader[E, T] =
		ask.bind(e => unit(sel(e)))
}

class Reader[E, T](val run : E => T) {

	require(run ne null, "run should not be null.")

	// '>>=' in Haskell
	def bind[S](f : T => Reader[E, S]) : Reader[E, S] =
		new Reader[E, S](e => f(run(e)).run(e))

	def local[D](f : D => E) : Reader[D, T] =
		new Reader[D, T](d => run(f(d)))
}
